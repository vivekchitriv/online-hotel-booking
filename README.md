# 1. The main objective of this project is to develop Online Hotel Bookings Management System (OHBMS).

# 2. This system can be used to search for Hotels and to book the rooms in that particular hotel

# 3. We want to provide comfort to the user in order that he can place order from any place and at any time.

## Technologies to build this project
 1. Spring Boot For Backend
 2. Angular  Front end
 3. My Sql Database

### TO RUN THIS PROJECT ON AWS

### URL : -  http://hms-frontend.s3-website-us-east-1.amazonaws.com/


## project Images
- 1 
![](images/Picture1.png)
- 2 
![](images/Picture2.png)
- 3 
![](images/Picture3.png)
- 4
![](images/Picture4.png)
- 5
![](images/Picture5.png)
- 6
![](images/Picture6.png)
- 7
![](images/Picture7.png)
- 8
![](images/Picture8.png)
- 9
![](images/Picture9.png)
- 10
![](images/Picture10.png)
- 11
![](images/Picture11.png)
- 12
![](images/Picture12.png)
- 13
![](images/Picture13.png)
- 14
![](images/Picture14.png)
- 15
![](images/Picture15.png)
- 16
![](images/Picture16.png)

