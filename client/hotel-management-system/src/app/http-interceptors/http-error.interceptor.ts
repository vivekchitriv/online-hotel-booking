import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { tap } from "rxjs/internal/operators/tap";

import { SpinnerService } from "../spinner/spinner.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor{
   
    constructor(private spinnerService:SpinnerService) {}
    intercept(request: HttpRequest<any>, next: HttpHandler) {
        
        this.spinnerService.requestStarted();
        return this.handler(next,request);
    }
    handler(next,request){

        return next.handle(request)
        this.spinnerService.requestEnded();
    }
    
}