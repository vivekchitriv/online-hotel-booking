import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment} from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RoomService {

  upadateRoomInformation: any;
  constructor(private http: HttpClient, public router: Router) { }



  adminRoomList() {
    return this.http.get<any>(environment.serviceRootUrl+'/getRoomList');

  }

  deleteRoomInfo(data) {
    console.log(data);
    return this.http.delete(environment.serviceRootUrl+`/deleteRoom/${data.roomId}`);
  }

  updateRoomInfo(data) {
    console.log(data);
    return this.http.put(environment.serviceRootUrl+'/updateRoomInforamtion', data);
  }

  addRoom(data): Observable<any> {
    console.log(data);
    return this.http.put<any>(environment.serviceRootUrl+'/addRoomInformation', data);
  }

  roomInfoForUpdation(roomInfo) {
    console.log('...........in hotel service..........', roomInfo);
    this.upadateRoomInformation = roomInfo;

    console.log('.......updateHotelInfo.......', this.upadateRoomInformation);
    this.router.navigateByUrl('/vertical-header/admin-update-room');
  }

}
