import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hotel } from './hotel-information';
import { Router } from '@angular/router';
import {environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  upadateEmployeeInformation: any;
  roomListForEmployee: any;
  constructor(private http: HttpClient, private router: Router) { }

  adminEmployeeList() {
    return this.http.get<any>(environment.serviceRootUrl+'/getEmployeeList');

  }

  deleteEmployeeInfo(data) {
    console.log(data);
    return this.http.delete(environment.serviceRootUrl+`/deleteEmployee/${data.employeeId}`);
  }

  updateEmployeeInfo(data) {
    console.log(data);
    return this.http.put(environment.serviceRootUrl+'/updateEmployeeInformation', data);
  }


  addEmployee(data): Observable<any> {
    console.log(data);
    return this.http.put<any>(environment.serviceRootUrl+'/addEmployee', data);
  }

  getHotelInfo(data): Observable<Hotel> {
    console.log('............getHotelInfo', data);
    return this.http.post<Hotel>(environment.serviceRootUrl+'/getHotelInfoForEmployee', data);
  }

  employeeRoomList(data): Observable<Hotel> {
    console.log('............employeeRoomList', data);
    return this.http.post<Hotel>(environment.serviceRootUrl+'/getRoomListForEmployee', data);
  }

  employeeBookRoom(data): Observable<any> {
    console.log(data);
    return this.http.put<any>(environment.serviceRootUrl+'/hotelBookingFromEmployee', data);
  }

  employeeInfoForUpdation(employeeInfo) {

    console.log('...........in hotel service..........', employeeInfo);
    this.upadateEmployeeInformation = employeeInfo;

    console.log('.......updateHotelInfo.......', this.upadateEmployeeInformation);
    this.router.navigateByUrl('/vertical-header/admin-update-employee');

  }

  employeeRoomListData(data) {
    this.roomListForEmployee = data;
  }
}
