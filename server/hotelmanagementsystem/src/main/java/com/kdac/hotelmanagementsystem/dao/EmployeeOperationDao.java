package com.kdac.hotelmanagementsystem.dao;

import java.util.List;

import com.kdac.hotelmanagementsystem.bean.AdminEmployeeUserBean;
import com.kdac.hotelmanagementsystem.bean.BookingInformationBean;
import com.kdac.hotelmanagementsystem.bean.HotelInformationBean;
import com.kdac.hotelmanagementsystem.bean.RoomInformationBean;

public interface EmployeeOperationDao {

	public HotelInformationBean getHotelInformation(AdminEmployeeUserBean adminEmployeeUserBean);

	public List<RoomInformationBean> getRoomList(AdminEmployeeUserBean adminEmployeeUserBean);

	public BookingInformationBean bookUser(BookingInformationBean bookingInformationBean);

}// end of interface
