package com.kdac.hotelmanagementsystem.dao;

import com.kdac.hotelmanagementsystem.bean.AdminEmployeeUserBean;

public interface AdminEmployeeUserDao {

	public AdminEmployeeUserBean adminEmployeeUserLogin(String email, String password);

	public AdminEmployeeUserBean userRegister(AdminEmployeeUserBean adminEmployeeUserBean);

	public boolean checkUserEmail(String hotelLicenceNumber);
	
	public AdminEmployeeUserBean userProfile(AdminEmployeeUserBean adminEmployeeUserBean);
	
	public AdminEmployeeUserBean updateUserProfile(AdminEmployeeUserBean adminEmployeeUserBean);
	
}// end of interface
