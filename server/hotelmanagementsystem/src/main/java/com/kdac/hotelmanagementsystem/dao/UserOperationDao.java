package com.kdac.hotelmanagementsystem.dao;

import java.util.List;

import com.kdac.hotelmanagementsystem.bean.AdminEmployeeUserBean;
import com.kdac.hotelmanagementsystem.bean.BookingInformationBean;
import com.kdac.hotelmanagementsystem.bean.HotelInformationBean;
import com.kdac.hotelmanagementsystem.bean.RoomInformationBean;
import com.kdac.hotelmanagementsystem.service.AdminOperationService;

public interface UserOperationDao {

	public List<RoomInformationBean> roomList(HotelInformationBean hotelInformationBean);

	public BookingInformationBean bookRoomUser(BookingInformationBean bookingInformationBean);

	public double calculateTotalDaysAmount(BookingInformationBean bookingInformationBean);
	
	public int updateRoomCount(int roomId);

	public String updateRoomStatus(int roomId);

	public List<BookingInformationBean> getBookingHistory(String email);

	public boolean cancelBooking(int bookingId);
	
}// end of interface
